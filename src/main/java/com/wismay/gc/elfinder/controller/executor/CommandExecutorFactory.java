package com.wismay.gc.elfinder.controller.executor;

public interface CommandExecutorFactory {
	CommandExecutor get(String commandName);
}