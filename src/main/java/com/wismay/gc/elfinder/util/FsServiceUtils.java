package com.wismay.gc.elfinder.util;

import java.io.IOException;

import com.wismay.gc.elfinder.controller.executor.FsItemEx;
import com.wismay.gc.elfinder.service.FsItem;
import com.wismay.gc.elfinder.service.FsService;

public abstract class FsServiceUtils {
	public static FsItemEx findItem(FsService fsService, String hash) throws IOException {
		FsItem fsi = fsService.fromHash(hash);
		if (fsi == null) {
			return null;
		}

		return new FsItemEx(fsi, fsService);
	}
}
