package com.wismay.gc.core.metamode;

import java.util.List;

public class MetaMode {
	private List<Table> tables;

	public MetaMode(List<Table> tables) {
		super();
		this.tables = tables;
	}

	public List<Table> getTables() {
		return tables;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}

}
