package com.wismay.gc.core.metamode;

public class Column {
	private String colName;
	private String typeName;//Data source dependent type name, for a UDT the type name is fully qualified
	private int type;//SQL type from java.sql.Types 
	private String comment;
	
	public Column(String colName, String typeName, int type, String comment) {
		super();
		this.colName = colName;
		this.typeName = typeName;
		this.type = type;
		this.comment = comment;
	}
	public String getColName() {
		return colName;
	}
	public void setColName(String colName) {
		this.colName = colName;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
}
