package com.wismay.gc.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.wismay.gc.entity.Project;
import com.wismay.gc.repository.ProjectDao;

//Spring Bean的标识.
@Component
// 默认将类中的所有public函数纳入事务管理.
@Transactional
public class ProjectService {

	@Autowired
	private ProjectDao projectDao;

	public Project getProject(Long id) {
		return projectDao.findOne(id);
	}

	public void saveProject(Project entity) {
		projectDao.save(entity);
	}

	public void deleteProject(Long id) {
		projectDao.delete(id);
	}

	public List<Project> getAllProject() {
		return (List<Project>) projectDao.findAll();
	}

}
