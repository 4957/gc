<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<head>
	<meta charset="utf-8">
	<title></title>
	<!-- The styles -->
	<link id="bs-css" href="${ctx}/static/charisma-master/css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link href="${ctx}/static/charisma-master/css/bootstrap-responsive.css" rel="stylesheet" />
	<link href="${ctx}/static/charisma-master/css/charisma-app.css" rel="stylesheet" />
	<link href="${ctx}/static/charisma-master/css/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
	<link href='${ctx}/static/charisma-master/css/fullcalendar.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/fullcalendar.print.css' rel='stylesheet'  media='print' />
	<link href='${ctx}/static/charisma-master/css/chosen.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/uniform.default.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/colorbox.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/jquery.cleditor.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/jquery.noty.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/noty_theme_default.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/elfinder.min.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/elfinder.theme.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/jquery.iphone.toggle.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/opa-icons.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/uploadify.css' rel='stylesheet' />

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="${ctx}/static/charisma-master/img/favicon.ico" />
		
</head>
</head>

<body>

	<div class="container-fluid">
		<div class="row-fluid">
		
			<div class="row-fluid">
				<div class="span12 center login-header">
					<br/>
					<br/>
					<h2>欢迎使用微码代码生成器 v1.0</h2>
					<h3>Wismay Java Code generator v1.0</h3>
				</div><!--/span-->
			</div><!--/row-->
			
			<br/>
			<br/>
			<br/>
			<div class="row-fluid">
				<div class="well span5 center login-box">
					<div class="alert alert-info">
						使用户名和密码登录系统
					</div>
					<%
					String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
					if(error != null){
					%>
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert">×</button>登录失败，请重试.
						</div>
					<%
					}
					%>
					<form id="loginForm" class="form-horizontal" action="${ctx}/login" method="post">
					
						<fieldset>
							<div class="input-prepend" title="Username" data-rel="tooltip">
								<span class="add-on"><i class="icon-user"></i></span><input autofocus class="input-large span10" name="username" id="username" type="text" value="admin" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" value="admin" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend">
							<label class="remember" for="remember"><input type="checkbox" id="remember" name="rememberMe" /> 记住我</label>
							</div>
							<div class="clearfix"></div>

							<p class="center span5">
							<button type="submit" class="btn btn-large btn-primary">Login</button>
							</p>
						</fieldset>
					</form>
				</div><!--/span-->
			</div><!--/row-->
				</div><!--/fluid-row-->
		
	</div><!--/.fluid-container-->
	
	<script>
		$(document).ready(function() {
			$("#loginForm").validate();
		});
	</script>
	
	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script src="${ctx}/static/charisma-master/js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script src="${ctx}/static/charisma-master/js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-tour.js"></script>
	<!-- library for cookie management-->
	<script src="${ctx}/static/charisma-master/js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='${ctx}/static/charisma-master/js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='${ctx}/static/charisma-master/js/jquery.dataTables.min.js'></script>

	<!-- chart libraries start -->
	<script src="${ctx}/static/charisma-master/js/excanvas.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.min.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.pie.min.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.stack.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.resize.min.js"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="${ctx}/static/charisma-master/js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="${ctx}/static/charisma-master/js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="${ctx}/static/charisma-master/js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="${ctx}/static/charisma-master/js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="${ctx}/static/charisma-master/js/jquery.elfinder.min.js"></script>
	<!-- star rating plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="${ctx}/static/charisma-master/js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="${ctx}/static/charisma-master/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="${ctx}/static/charisma-master/js/charisma.js"></script>
</body>
</html>
