<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<script type="text/javascript">
	//jquery.uploadify
	/***
	$(function() {
	    $("#driverFile").uploadify({
	    	debug           : false,
	        height          : 30,
	        swf             : '${ctx}/static/uploadify/uploadify.swf',
	        uploader        : '${ctx}/data-source/ajax/upload-dirver-file/${projectId}',
	        width           : 120,
	        file_post_name  : 'driverFile',
	        formData : { '<?php echo session_name();?>' : '<?php echo session_id();?>' },
	        onUploadSuccess : function(){
	        	$.ajax({
	    			url: "${ctx}/data-source/ajax/detail/${projectId}",
	    		    cache: true
	    		}).done(function( html ) {
	    		   $("#data-source-detail").html(html);
	    		});
	        }
	    });
	});
	***/
	
	var uploader = WebUploader.create({
		
		// 选完文件后，是否自动上传。
	    auto: true,

	    // swf文件路径
	    swf: '${ctx}/static/webuploader/Uploader.swf',

	    // 文件接收服务端。
	    server: '${ctx}/webupload/ajax/upload-dirver-file-for-webuploader/${projectId}',

	    // 选择文件的按钮。可选。
	    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
	    pick: '#picker',

	    // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
	    resize: false
	});
	
	// 当有文件被添加进队列的时候
	uploader.on( 'fileQueued', function( file ) {
		var $list = $( '#thelist');
	    $list.append( '<div id="' + file.id + '" class="item">' +
	        '<h4 class="info">' + file.name + '</h4>' +
	        '<p class="state">等待上传...</p>' +
	    '</div>' );
	});
	
	// 文件上传过程中创建进度条实时显示。
	uploader.on( 'uploadProgress', function( file, percentage ) {
	    var $li = $( '#'+file.id ),
	        $percent = $li.find('.progress .progress-bar');

	    // 避免重复创建
	    if ( !$percent.length ) {
	        $percent = $('<div class="progress progress-striped active">' +
	          '<div class="progress-bar" role="progressbar" style="width: 0%">' +
	          '</div>' +
	        '</div>').appendTo( $li ).find('.progress-bar');
	    }

	    $li.find('p.state').text('上传中');

	    $percent.css( 'width', percentage * 100 + '%' );
	});

	uploader.on( 'uploadSuccess', function( file ) {
		$( '#'+file.id ).find('p.state').text('已上传');
		$.ajax({
			url: "${ctx}/data-source/ajax/detail/${projectId}",
		    cache: true
		}).done(function( html ) {
		   $("#data-source-detail").html(html);
		});
	    
	});

	uploader.on( 'uploadError', function( file ) {
	    $( '#'+file.id ).find('p.state').text('上传出错');
	});

	uploader.on( 'uploadComplete', function( file ) {
	    //$( '#'+file.id ).find('.progress').fadeOut();
	});




	
	
	//测试数据库连接
	function testDatabaseConnect(projectId){
		$.ajax({
			url: "${ctx}/data-source/ajax/test-db-connect/"+projectId,
		    cache: true
		}).done(function( data ) {
			var html='';
			if(data.result){
				html="<div class='alert alert-success'>"+
					"<button type='button' class='close' data-dismiss='alert'>×</button>"+
					"<strong>连接成功！</strong> "+
				"</div> ";
			}else{
				html="<div class='alert alert-error'>"+
					"<button type='button' class='close' data-dismiss='alert'>×</button>"+
					"<strong>连接失败！<br/>"+data.message+"</strong>"+
				"</div>";
			}
			
		   $("#testResult").html(html);
		   
		});
	}
	
	
</script>	
<div class="box-content">
	<table class="table">
		<tr>
			<td><i class="icon-cog"></i> <span class="blue"><b> 驱动名称</b></span></td>
			<td colspan="2"><code>${dataSource.driver}</code></td>
		</tr>
		<tr>
			<td><i class="icon-refresh"></i> <span class="blue"><b> 链接url</b></span></td>
			<td colspan="2"><code>${dataSource.url}</code></td>
		</tr>
		<tr>
			<td><i class="icon-user"></i> <span class="blue"><b> 用户名</b></span></td>
			<td colspan="2"><code>${dataSource.username}</code></td>
		</tr>
		<tr>
			<td><i class="icon-th"></i> <span class="blue"><b> 密码</b></span></td>
			<td colspan="2"><code>${dataSource.password}</code></td>
		</tr>
		<tr>
			<td><i class="icon-cog"></i> <span class="blue"><b> 驱动文件</b></span></td>
			<td><code>${dataSource.driverFileName}</code></td>
			<td width="140">
				
				<!--dom结构部分-->
				<div id="uploader" class="wu-example">
				    <!--用来存放文件信息-->
				    <div id="thelist" class="uploader-list"></div>
				    <div class="btns">
				        <div id="picker">选择文件</div>
				    </div>
				</div>
			
			</td>
		</tr>
	</table>
	

	<div class="center">
		<a href="javascript:testDatabaseConnect(${projectId});" class="btn btn-large">测试连接</a>
		<div id="testResult"></div>
	</div>
</div>
