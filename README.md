# Java 代码生成器
- 自定义源码格式，根据程序猿自定义的freemarker模板生成源代码。
- 数据源目前只支持Mysql

# 运行
- 下载代码到Eclipse
- 下载 springside4.1，然后运行 quick-start.bat
- 通过命令行在项目根目录下运行mvn eclipse:eclipse命令
- 到Eclipse里刷新项目，然后运行src/test/java com.wismay.gc.QuickStartServer.java里的main方法
- 然后在浏览器里访问：http://localhost:8080/gc
- 然后 你就可以尽情的偷懒了 ):

# 使用到的技术
- 基于[springside4]( http://www.springside.org.cn/)
- 使用[Freemarker](http://freemarker.org/)作为源码的模板
- [H2](http://www.h2database.com/)数据库

# 界面截图
![image](http://git.oschina.net/solarisy/gc/raw/master/src/main/webapp/static/images/生成代码.png)